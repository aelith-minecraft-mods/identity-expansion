package aelita.identity_expansion;

import aelita.identity_expansion.power.IdentityPower;
import io.github.edwinmindcraft.apoli.api.power.factory.PowerFactory;
import io.github.edwinmindcraft.apoli.api.registry.ApoliRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.RegistryObject;

public class Powers {
	public static final DeferredRegister<PowerFactory<?>> REGISTRY
		= DeferredRegister.create(ApoliRegistries.POWER_FACTORY_KEY, IdentityExpansionMod.MOD_ID);

	public static final RegistryObject<IdentityPower> UNLOCK_IDENTITY = REGISTRY.register("unlock_identity", IdentityPower::new);
}
