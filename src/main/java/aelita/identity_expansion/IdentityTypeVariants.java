package aelita.identity_expansion;

import java.util.function.Supplier;

import aelita.identity_expansion.identity.RabbitTypeProvider;
import aelita.identity_expansion.mixin.IdentityTypeAccessor;
import draylar.identity.api.variant.TypeProvider;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegisterEvent;

public class IdentityTypeVariants
{
	private static void registerIdentityTypeVariants() {
		addTypeProvider(EntityType.RABBIT, RabbitTypeProvider::new);
	}

	private static <T extends LivingEntity> void addTypeProvider(EntityType<T> entityType, Supplier<TypeProvider<T>> typeProvider) {
		IdentityTypeAccessor.getVariantsByType().put(entityType, typeProvider.get());
	}

	@EventBusSubscriber(modid = IdentityExpansionMod.MOD_ID, bus = Bus.MOD)
	private static class RegisterEventSubscriber
	{
		@SubscribeEvent(priority = EventPriority.LOWEST)
		public static void onRegister(final RegisterEvent event) {
			if (event.getRegistryKey() != ForgeRegistries.Keys.ENTITY_TYPES) {
				return;
			}
			registerIdentityTypeVariants();
		}
	}
}
