package aelita.identity_expansion.identity;

import com.google.common.collect.ImmutableMap;

import draylar.identity.api.variant.TypeProvider;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.animal.Rabbit;
import net.minecraft.world.level.Level;

public class RabbitTypeProvider extends TypeProvider<Rabbit> {
	private static final ImmutableMap<Integer, String> PREFIX_BY_ID = ImmutableMap.of(
		0, "Brown",
		1, "White",
		2, "Black",
		3, "Black and White",
		4, "Gold",
		5, "Salt and Pepper"
	);

	public RabbitTypeProvider() {}

	@Override
	public Rabbit create(EntityType<Rabbit> entityType, Level world, int rabbitType) {
		Rabbit bunny = new Rabbit(entityType, world);
		bunny.setRabbitType(rabbitType);
		return bunny;
	}

	@Override
	public int getFallbackData() {
		return 0;
	}

	@Override
	public int getRange() {
		return 5;
	}

	@Override
	public int getVariantData(Rabbit bunny) {
		return bunny.getRabbitType();
	}

	@Override
	public Component modifyText(Rabbit bunny, MutableComponent text) {
		int rabbitType = getVariantData(bunny);
		return Component.literal(PREFIX_BY_ID.containsKey(rabbitType)
			? PREFIX_BY_ID.get(rabbitType) + " "
			: ""
		).append(text);
	}
}
