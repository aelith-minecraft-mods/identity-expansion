package aelita.identity_expansion.mixin;

import java.util.Map;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

import draylar.identity.api.variant.IdentityType;
import draylar.identity.api.variant.TypeProvider;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;

@Mixin(IdentityType.class)
public interface IdentityTypeAccessor {
	@Accessor("VARIANT_BY_TYPE")
	public static Map<EntityType<? extends LivingEntity>, TypeProvider<?>> getVariantsByType() {
		throw new AssertionError();
	}
}
