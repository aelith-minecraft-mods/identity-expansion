package aelita.identity_expansion.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import aelita.identity_expansion.power.IdentityPower;
import draylar.identity.api.PlayerUnlocks;
import draylar.identity.api.variant.IdentityType;
import net.minecraft.world.entity.player.Player;

@Mixin(PlayerUnlocks.class)
public abstract class PlayerUnlocksMixin
{
	@Inject(method = "has", at = @At("RETURN"), cancellable = true)
	private static void hasMixin(Player player, IdentityType<?> identity, CallbackInfoReturnable<Boolean> callbackInfo) {
		if (IdentityPower.enablesIdentity(player, identity)) {
			callbackInfo.setReturnValue(true);
		}
	}
}
