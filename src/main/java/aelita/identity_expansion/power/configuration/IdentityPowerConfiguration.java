package aelita.identity_expansion.power.configuration;

import java.util.ArrayList;
import java.util.List;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import io.github.apace100.calio.data.SerializableDataType;
import io.github.apace100.calio.data.SerializableDataTypes;
import io.github.edwinmindcraft.apoli.api.IDynamicFeatureConfiguration;
import io.github.edwinmindcraft.calio.api.network.CalioCodecHelper;
import net.minecraft.world.entity.EntityType;

public record IdentityPowerConfiguration(
	List<EntityType<?>> entityTypes
) implements IDynamicFeatureConfiguration {
	public static final Codec<IdentityPowerConfiguration> CODEC = RecordCodecBuilder.create(
		instance -> instance.group(
			CalioCodecHelper.optionalField(
				SerializableDataType.list(SerializableDataTypes.ENTITY_TYPE),
				"entity_types",
				new ArrayList<EntityType<?>>()
			).forGetter(IdentityPowerConfiguration::entityTypes)
		).apply(instance, (entityTypes) -> new IdentityPowerConfiguration(entityTypes))
	);
}
