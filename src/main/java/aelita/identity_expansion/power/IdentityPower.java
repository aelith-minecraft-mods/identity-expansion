package aelita.identity_expansion.power;

import java.util.List;

import aelita.identity_expansion.Powers;
import aelita.identity_expansion.power.configuration.IdentityPowerConfiguration;
import draylar.identity.api.variant.IdentityType;
import io.github.edwinmindcraft.apoli.api.component.IPowerContainer;
import io.github.edwinmindcraft.apoli.api.power.configuration.ConfiguredPower;
import io.github.edwinmindcraft.apoli.api.power.factory.PowerFactory;
import net.minecraft.core.Holder;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;

public class IdentityPower extends PowerFactory<IdentityPowerConfiguration> {
	public IdentityPower() {
		super(IdentityPowerConfiguration.CODEC);
	}

	public boolean enablesIdentity(
		ConfiguredPower<IdentityPowerConfiguration, ?> power,
		Entity entity,
		IdentityType<?> identity
	) {
		final IdentityPowerConfiguration config = power.getConfiguration();
		final EntityType<? extends LivingEntity> entityType = identity.getEntityType();
		if (config.entityTypes().contains(entityType)) {
			return true;
		}
		return false;
	}

	public static boolean enablesIdentity(Entity player, IdentityType<?> identity) {
		List<Holder<ConfiguredPower<IdentityPowerConfiguration, IdentityPower>>> identityPowers
			= IPowerContainer.getPowers(player, Powers.UNLOCK_IDENTITY.get());

		for (final Holder<ConfiguredPower<IdentityPowerConfiguration, IdentityPower>> holder : identityPowers) {
			ConfiguredPower<IdentityPowerConfiguration, IdentityPower> power = holder.get();
			if (power.getFactory().enablesIdentity(power, player, identity)) {
				return true;
			}
		}
		return false;
	}
}
