package aelita.identity_expansion;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(IdentityExpansionMod.MOD_ID)
public class IdentityExpansionMod {
	public static final String MOD_ID = "aelitas_identity_expansion";
	public static final Logger LOGGER = LogManager.getLogger(IdentityExpansionMod.MOD_ID);

	public IdentityExpansionMod() {
		IEventBus modEventBus = FMLJavaModLoadingContext.get().getModEventBus();
		initRegistries(modEventBus);
	}

	private void initRegistries(IEventBus bus) {
		// All registries must be hooked up this way:
		// Blocks.REGISTRY.register(bus);

		// Apoli registries
		Powers.REGISTRY.register(bus);
	}
}
